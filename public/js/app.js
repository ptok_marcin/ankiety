const disabledA = document.querySelectorAll('a.action-button.disabled');
disabledA.forEach((el) => {
  el.addEventListener('click', (e) => e.preventDefault());
});

const prevBtn = document.querySelector('.prev');
const nextBtn = document.querySelector('.next');
const steps = document.querySelectorAll('.quiz-step');
const progressIndicator = document.querySelector('.progress-indicator');
const numOfSteps = steps.length;
let currentStep = 1;

const hideSteps = () => {
  steps.forEach((step) => {
    if (step.dataset.pos == currentStep) step.style.display = step.classList.contains('question-element') ? ' block' : 'flex';
    else step.style.display = 'none';
  });
};

const prepareUI = () => {
  if (currentStep >= numOfSteps) {
    nextBtn.disabled = true;
    prevBtn.disabled = false;
    nextBtn.classList.add('disabled');
    prevBtn.classList.remove('disabled');
  } else if (currentStep <= 1) {
    prevBtn.disabled = true;
    prevBtn.classList.add('disabled');

    nextBtn.disabled = false;
    nextBtn.classList.remove('disabled');
  } else {
    nextBtn.disabled = false;
    nextBtn.classList.remove('disabled');

    prevBtn.disabled = false;
    prevBtn.classList.remove('disabled');
  }
  hideSteps();
  progressIndicator.style.width = `${(currentStep / numOfSteps) * 100}%`;
};

const nextStep = () => {
  currentStep = currentStep >= numOfSteps ? numOfSteps : ++currentStep;
  prepareUI();
};

const prevStep = () => {
  currentStep = currentStep <= 1 ? 1 : --currentStep;
  prepareUI();
};

prevBtn.addEventListener('click', prevStep);
nextBtn.addEventListener('click', nextStep);

prepareUI();
